<?php

namespace Drupal\agoramobilemenu\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Menu\InaccessibleMenuLink;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides agoramobilemenu block.
 */
#[Block(
  id: "agoramobilemenu",
  admin_label: new TranslatableMarkup("Mobile Menu"),
  category: new TranslatableMarkup('agoradesign'),
)]
class Agoramobilemenu extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The menu tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected MenuLinkTreeInterface $menuTree;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $config;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs an Agoramobilemenu object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menuTree
   *   The menu tree.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, MenuLinkTreeInterface $menuTree, Connection $connection, ConfigFactoryInterface $config, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->menuTree = $menuTree;
    $this->connection = $connection;
    $this->config = $config;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $items = $this->loadMobileMenuItems();

    $cache_tags = [];
    foreach ($this->getMenuIds() as $menu_name) {
      $cache_tags[] = 'config:system.menu.' . $menu_name;
    }

    return [
      '#theme' => 'agoramobilemenu',
      '#items' => $items,
      '#cache' => [
        'tags' => $cache_tags,
      ],
    ];
  }

  /**
   * Loads mobile menu items.
   *
   * @return array
   *   The loaded mobile menu items.
   */
  protected function loadMobileMenuItems(): array {
    $result = [];
    $menus_to_marge = $this->getMenuIds();
    if (!empty($menus_to_marge)) {
      $mobile_menu_tree = $this->mergeMenus($menus_to_marge);
      $mobile_menu = $this->menuTree->build($mobile_menu_tree);
      $result = !empty($mobile_menu['#items']) ? $mobile_menu['#items'] : [];
    }
    return $result;
  }

  /**
   * Returns a list of menu IDs that should be included in the mobile menu.
   *
   * @return string[]
   *   A list of menu IDs that should be included in the mobile menu.
   */
  protected function getMenuIds(): array {
    return $this->config->get('agoramobilemenu.settings')->get('menus');
  }

  /**
   * Builds a merged menu tree for the given menu names.
   *
   * @param array $menu_names
   *   The menu names.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   A merged menu tree for the given menu names.
   */
  protected function mergeMenus(array $menu_names = []): array {
    $result = [];
    if (!empty($menu_names) && is_array($menu_names)) {
      $item_array = [];
      $menu_base_weight = 0;
      $use_global_sorting = $this->config->get('agoramobilemenu.settings')
        ->get('global_sorting');
      foreach ($menu_names as $menu_name) {
        if (!$use_global_sorting) {
          $menu_base_weight += 100;
        }
        $tree = $this->getMenuItemsTree($menu_name);
        foreach ($tree as $item) {
          if ($item->link->isEnabled() && !($item->link instanceof InaccessibleMenuLink)) {
            if ($menu_base_weight) {
              $original_weight = $item->link->getWeight();
              $item->link->updateLink(['weight' => $menu_base_weight + $original_weight], FALSE);
            }
            $item_array[] = $item;
          }
        }
      }

      /*
       * TODO Testen, ob man den generateIndexAndSort Manipulator auch gleich in
       * getMenuItemsTree() machen kann (-> wir verändern ja z.T. die weight
       * property danach).
       */
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];
      $this->moduleHandler->alter('agoramobilemenu_menu_link_tree_manipulators', $manipulators);
      $result = $this->menuTree->transform($item_array, $manipulators);
    }
    return $result;
  }

  /**
   * Builds a menu link tree for the given menu (name).
   *
   * @param string $menu_name
   *   The menu name.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   The built menu link tree for the given menu name.
   */
  protected function getMenuItemsTree(string $menu_name): array {
    $result = [];
    if (!empty($menu_name)) {
      $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name);
      $parentsToExpand = $this->getAllParents($menu_name);
      if (!empty($parentsToExpand)) {
        $parameters->addExpandedParents($parentsToExpand);
      }
      $tree = $this->menuTree->load($menu_name, $parameters);
      $manipulators = [['callable' => 'menu.default_tree_manipulators:checkAccess']];
      $result = $this->menuTree->transform($tree, $manipulators);
    }
    return $result;
  }

  /**
   * Finds all links in a menu given a set of possible parents having children.
   *
   * In contrary to MenuTreeStorage::getExpanded, the 'expanded' property is not
   * a criterion.
   *
   * @param string $menu_name
   *   The menu name.
   *
   * @return array
   *   The parents items.
   *
   * @see \Drupal\Core\Menu\MenuTreeStorage::getExpanded
   */
  protected function getAllParents(string $menu_name): array {
    $parents = [];
    $table = 'menu_tree';
    do {
      $query = $this->connection->select($table, NULL);
      $query->fields($table, ['id']);
      $query->condition('menu_name', $menu_name);
      $query->condition('has_children', 1);
      $query->condition('enabled', 1);
      if (empty($parents)) {
        $query->condition('depth', 1);
      }
      else {
        $query->condition('parent', $parents, 'IN');
        $query->condition('id', $parents, 'NOT IN');
      }
      $result = $query->execute()->fetchAllKeyed(0, 0);
      $parents += $result;
    } while (!empty($result));
    return $parents;
  }

}
