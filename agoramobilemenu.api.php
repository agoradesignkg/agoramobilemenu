<?php

/**
 * @file
 * Hooks provided by the agoramobilemenu module.
 */

/**
 * Alter the result of the menu link tree manipulators used for mobile nav.
 *
 * @param array $manipulators
 *   An array of callable definitions of menu link manipulators.
 */
function hook_agoramobilemenu_menu_link_tree_manipulators_alter(array &$manipulators) {
  $manipulators[] = ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'];
}
